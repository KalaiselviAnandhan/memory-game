const gameContainer = document.getElementById("game");

const GIF = [
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/7.gif",
    "./gifs/8.gif",
    "./gifs/9.gif",
    "./gifs/10.gif",
    "./gifs/11.gif",
    "./gifs/12.gif",
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/7.gif",
    "./gifs/8.gif",
    "./gifs/9.gif",
    "./gifs/10.gif",
    "./gifs/11.gif",
    "./gifs/12.gif"
];
let classnames = [];
let identifier = [];
let turns = 0,total=0;

function shuffle(array) {
    let counter = array.length;

    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);
        counter--;

        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
}

    return array;
}

let shuffledColors = shuffle(GIF);

function createDivsForColors(colorArray) {
    let count=0;
    for (let color of colorArray) {
        count++;
        const newDiv = document.createElement("div");
        newDiv.classList.add(color);

        newDiv.setAttribute("id","gif"+count);

        newDiv.addEventListener("click", handleCardClick);

        gameContainer.append(newDiv);
    }
}

function handleCardClick(event) {
    
    const card = event.target;
    const gif = card.classList.value;
    
    if(classnames.length < 2 && card.classList.contains('constant') == false){
        card.style.backgroundImage = `url(${gif})`;

        if(classnames.length == 0){
            classnames.push(gif);
            identifier.push(card.id);
            turns++;
        }
        else{

            if(classnames.length == 1){
                classnames.push(gif);
                identifier.push(card.id);

                if(classnames[0] === classnames[1] && identifier[0] !== identifier[1]){
                    total += 2;
                    turns++;
                    let card1 = document.getElementById(identifier[0]);
                    let card2 = document.getElementById(identifier[1]);
                    card1.classList.add('constant');
                    card2.classList.add('constant');
                    classnames = [];
                    identifier = [];
                }
                else{

                    if(identifier[0] !== identifier[1]){
                        turns++;

                        setTimeout(() => {
                            const card1 = document.getElementById(identifier[0]);
                            const card2 = document.getElementById(identifier[1]);
                            if(card1 != null && card2 != null){
                                card1.style.backgroundImage = `url("./image/pooh-bear.jpg")`
                                card2.style.backgroundImage = `url("./image/pooh-bear.jpg")`
                            }
                            classnames = [];
                            identifier = [];
                        },1000);
                    }
                }
            }
        }
    }
    else{
        identifier.forEach((element)=>{
            let card=document.getElementById(element);
            if(!card.classList.contains('constant')){
                card.style.backgroundImage = `url("./image/pooh-bear.jpg")`;
            }
        })
        classnames = [];
        identifier = [];
    }
    score(total,turns)
}

function score(total,turns){
    if(total==24){
        alert(`You Won the Game ✨  with ${turns} turns..`);
        let leastTurns = ((localStorage.getItem("flips") == null) || (localStorage.getItem("flips") >= turns)) ? (function(){
            localStorage.setItem("flips",turns);
            return turns;
        })():localStorage.getItem("flips");

        const div = document.getElementById("leastTurns");
        div.appendChild(document.createTextNode(`LeastTurns : ${leastTurns} Your Turns: ${turns}`));
        
    }
}

createDivsForColors(shuffledColors);